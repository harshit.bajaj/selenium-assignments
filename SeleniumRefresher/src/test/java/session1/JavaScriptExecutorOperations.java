package session1;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class JavaScriptExecutorOperations {
	
	public static void main(String[] args) throws Exception {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.automationtesting.in/Register.html");
//		Actions act = new Actions(driver);
//		act.sendKeys(Keys.ARROW_DOWN).perform();
//		Thread.sleep(3000);
//		act.sendKeys(Keys.ARROW_DOWN).perform();
		
		JavascriptExecutor js=(JavascriptExecutor)driver;
        js.executeScript("document.getElementById('yearbox').scrollIntoView()");
        
        
        WebElement ele=driver.findElement(By.xpath("//input[@ng-model='FirstName']"));
        js.executeScript("arguments[0].value='harshit'", ele);
	}
}

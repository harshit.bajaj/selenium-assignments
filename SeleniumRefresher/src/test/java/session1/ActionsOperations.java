package session1;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class ActionsOperations {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		WebDriver driver = new ChromeDriver();
		String parent = driver.getWindowHandle();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/login");
		driver.findElement(By.id("Email")).sendKeys("harshit3@yopmail.com");
		driver.findElement(By.id("Password")).sendKeys("1234..Hart");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		WebElement books = driver.findElement(By.xpath("//ul[@class='top-menu']//child::a[@href='/books']"));
		WebElement computers = driver.findElement(By.xpath("//ul[@class='top-menu']//child::a[@href='/computers']"));
		Actions act = new Actions(driver);
		act.moveToElement(computers).build().perform();
		Thread.sleep(3000);
		books.click();
		
		Select select = new Select(driver.findElement(By.id("products-orderby")));
		select.selectByVisibleText("Price: Low to High");
		select = new Select(driver.findElement(By.id("products-orderby")));
		select.selectByValue("https://demowebshop.tricentis.com/books?orderby=6");
		select = new Select(driver.findElement(By.id("products-orderby")));
		select.selectByIndex(5);
		
		act.contextClick(driver.findElement(By.xpath("//span[text()='Wishlist']"))).build().perform();
//	    act.sendKeys(Keys.DOWN);
//	    act.sendKeys(Keys.ENTER).build().perform();
		Robot robo = new Robot();
		robo.keyPress(KeyEvent.VK_DOWN);
		robo.keyRelease(KeyEvent.VK_DOWN);
		robo.keyPress(KeyEvent.VK_DOWN);
		robo.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(3000);
		robo.keyPress(KeyEvent.VK_ENTER);
		robo.keyRelease(KeyEvent.VK_ENTER);
		
		Set<String> set = driver.getWindowHandles();
		for(String s: set) {
			if(!s.equals(parent)) {
				driver.switchTo().window(s);
				driver.close();
			}
		}
		driver.switchTo().window(parent);
		
	}
}
